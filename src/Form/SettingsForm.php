<?php

namespace Drupal\conditional_memory\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'conditional_memory_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['conditional_memory.settings'];
  }

  /**
   * Holds the name of the keys we hold in the variable.
   */
  public function defaultKeys() {
    return [
      'enabled',
      'memory',
      'urls',
      'ignore_langcode',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('conditional_memory.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Enable conditional memory.'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['memory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Memory'),
      '#description' => $this->t('Memory that should be set for the configured urls. For example: 128M, 512M'),
      '#default_value' => $config->get('memory'),
    ];

    $form['urls'] = [
      '#type' => 'textarea',
      '#title' => $this->t('URLs'),
      '#description' => $this->t('The URLs for which memory should be increased. Urls should have a leading slash - E.g: <b> /admin/test. </b> Please enter each url in a new line.'),
      '#default_value' => $config->get('urls'),
    ];

    $form['ignore_langcode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ignore langcode'),
      '#description' => $this->t('Enable this if you want the langcode in the path to be ignored.'),
      '#default_value' => $config->get('enabled'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('conditional_memory.settings');

    foreach ($this->defaultKeys() as $key) {
      $config->set($key, $form_state->getValue($key));
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
