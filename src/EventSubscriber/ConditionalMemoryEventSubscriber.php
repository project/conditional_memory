<?php

namespace Drupal\conditional_memory\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\language\ConfigurableLanguageManagerInterface;

/**
 * Class ConditionalMemoryEventSubscriber.
 *
 * Increase memory conditionally for specific urls.
 */
class ConditionalMemoryEventSubscriber implements EventSubscriberInterface {

  /**
   * The factory for configuration objects.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcher
   */
  protected $pathMatcher;

  /**
   * The language manager service.
   *
   * @var \Drupal\language\ConfigurableLanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs an ConditionalMemoryEventSubscriber object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher.
   * @param \Drupal\language\ConfigurableLanguageManagerInterface $language_manager
   *   The language manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PathMatcherInterface $path_matcher, ConfigurableLanguageManagerInterface $language_manager) {
    $this->configFactory = $config_factory;
    $this->pathMatcher = $path_matcher;
    $this->languageManager = $language_manager;
  }

  /**
   * Increase memory for configured urls.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The event to process.
   */
  public function onRequest(GetResponseEvent $event) {
    $config = $this->configFactory->get('conditional_memory.settings');
    if ($config->get('enabled') && !empty($config->get('urls'))) {
      $urls = $config->get('urls');
      $request = $event->getRequest();
      $current_path = $request->getPathInfo();
      if ($config->get('ignore_langcode')) {
        $langcode = $this->languageManager->getCurrentLanguage()->getId();
        if (strpos($current_path, '/' . $langcode) === 0) {
          $current_path = substr($current_path, (strlen($langcode) + 1));
        }
      }
      if ($this->pathMatcher->matchPath($current_path, $urls)) {
        ini_set('memory_limit', $config->get('memory'));
      }
    }
  }

  /**
   * Implements EventSubscriberInterface::getSubscribedEvents().
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents() {
    // Setting high priority for this subscription in order to execute it soon
    // enough.
    $events[KernelEvents::REQUEST][] = ['onRequest', 100];

    return $events;
  }

}
